package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 */
public class Controller {

    /* 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
    private final String DEFAULT_FILE_PATH = System.getProperty("user.home") + System.getProperty("file.separator") + "output.txt";
    private File currentFile;
    /**
     * Constructs a new Controller with the default file as current.
     * */
    public Controller() {
        currentFile = new File(DEFAULT_FILE_PATH);
    }
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     */ 
    /**
     * Sets the specified file as the current file.
     * 
     * @param file the file to set as current
     * */
    public void setCurrentFile(final File file) {
        this.currentFile = file;
    }
    /* 2) A method for getting the current File
     */ 
    /**
     * Gets the current file.
     * 
     * @return The current file
     * */
    public File getCurrentFile() {
        return this.currentFile;
    }
    /* 3) A method for getting the path (in form of String) of the current File
     */ 
    /**
     * Gets the current file's path.
     * 
     * @return The file's path in string representation
     * */
    public String getCurrentPath() {
        return this.currentFile.getPath();
    }
    /* 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     */ 
    /**
     * Writes the given string to the current file.
     * 
     * @param string the string to write.
     * @throws IOException in case of file writing error.
     * */
    public void printToFile(final String string) throws IOException {
        try (FileWriter writer = new FileWriter(currentFile)) {
            writer.write(string);
            writer.flush();
        } catch (IOException ex) {
            throw ex;
        }
    }

}
