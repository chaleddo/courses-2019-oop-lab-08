package it.unibo.oop.lab.advanced;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.io.IOException;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private final DrawNumber model;
    private final DrawNumberView view;
    /**/
    private static class SettingsLoader {
        private int min;
        private int max;
        private int attempts;
        /**/
        public void load() throws IOException {
            final String path = System.getProperty("user.home");
            final List<String> lines = Files.readAllLines(Path.of(path));
            for (String line : lines) {
                line = line.substring(line.indexOf(':' + 1));
            }
            this.min = Integer.parseInt(lines.get(0));
            this.max = Integer.parseInt(lines.get(1));
            this.attempts = Integer.parseInt(lines.get(2));
        }
        public int getMin() {
            return this.min;
        }
        public int getMax() {
            return this.max;
        }
        public int getAttempts() {
            return this.attempts;
        }
    }
    /**
     * Creates a new App.
     * */
    public DrawNumberApp() {
        this.view = new DrawNumberViewImpl();
        try {
            SettingsLoader settings = new SettingsLoader();
            settings.load();
            this.model = new DrawNumberImpl(settings.getMin(), settings.getMax(), settings.getAttempts());
            this.view.setObserver(this);
            this.view.start();
        } catch (IOException e) {
            view.displayError(e.getMessage());
        }
    }
    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            this.view.result(result);
        } catch (IllegalArgumentException e) {
            this.view.numberIncorrect();
        } catch (AttemptsLimitReachedException e) {
            view.limitsReached();
        }
    }
    @Override
    public void resetGame() {
        this.model.reset();
    }
    @Override
    public void quit() {
        System.exit(0);
    }
    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp();
    }

}
