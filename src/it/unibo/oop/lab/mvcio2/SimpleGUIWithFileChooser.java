package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    private final JFrame frame = new JFrame();
    private final Controller controller = new Controller();
    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save" right
     * below (see "ex02.png" for the expected result). SUGGESTION: Use a JPanel with
     * BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The program asks the controller to save the file if the button "Save" gets
     * pressed.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */

    /**
     * builds a new {@link SimpleGUIWithFileChooser}.
     */
    public SimpleGUIWithFileChooser() {
        /*Build the frame's component.*/
        final JPanel canvas = new JPanel();
        canvas.setLayout(new BorderLayout());
        /* Text */
        final JTextArea text = new JTextArea();
        canvas.add(text, BorderLayout.CENTER);
        /* Save */
        final JButton save = new JButton("Save");
        canvas.add(save, BorderLayout.SOUTH);
        /* 1) Add a JTextField and a button "Browse..." on the upper part of the
         * graphical interface.
         * Suggestion: use a second JPanel with a second BorderLayout, put the panel
         * in the North of the main panel, put the text field in the center of the
         * new panel and put the button in the line_end of the new panel.
         */
        final JPanel fileCanvas = new JPanel();
        fileCanvas.setLayout(new BorderLayout());
        /* File path */
        final JTextField file = new JTextField();
        file.setText(controller.getCurrentPath());
        /* 2) The JTextField should be non modifiable. And, should display the
         * current selected file.
         */ 
        file.setEditable(false);
        fileCanvas.add(file, BorderLayout.CENTER);
        /* Browse file */
        final JButton browse = new JButton("Browse...");
        fileCanvas.add(browse, BorderLayout.LINE_END);
        canvas.add(fileCanvas, BorderLayout.NORTH);
        /* Events */
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    controller.printToFile(text.getText());
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Save Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });
        browse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                /* 3) On press, the button should open a JFileChooser. The program should
                * use the method showSaveDialog() to display the file chooser, and if the
                * result is equal to JFileChooser.APPROVE_OPTION the program should set as
                * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
                * then the program should do nothing. Otherwise, a message dialog should be
                * shown telling the user that an error has occurred (use
                * JOptionPane.showMessageDialog()).
                */
                final JFileChooser fileChooser = new JFileChooser();
                final int result = fileChooser.showSaveDialog(frame);
                if (result == JFileChooser.APPROVE_OPTION) {
                    /* 4) When in the controller a new File is set, also the graphical interface
                     * must reflect such change. Suggestion: do not force the controller to
                     * update the UI: in this example the UI knows when should be updated, so
                     * try to keep things separated.
                     */
                    controller.setCurrentFile(fileChooser.getSelectedFile());
                    file.setText(controller.getCurrentPath());
                } else if (result != JFileChooser.CANCEL_OPTION) {
                    JOptionPane.showMessageDialog(frame, "An error has occurred.", "Broswe Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(canvas);
    }
    /**
     * Displays the GUI onscreen.
     * */
    public void display()
    {
        frame.pack();
        frame.setVisible(true);
    }
    /**
     * main method.
     * 
     * @param args ignored
     * */
    public static void main(final String[] args) {
        new SimpleGUIWithFileChooser().display();
    }
}
